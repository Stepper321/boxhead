﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InControl;

public class PlayerController : MonoBehaviour, IDamageAble {

    
    
    //Player stats
    float player_hp = 100;
    public sbyte playerNumber = 0;
    public bool hasCamera = false;
    public bool dead { get; private set; }
    InventoryController inventoryController;
    
    //Weapons
    [SerializeField]
    private GameObject weaponHolder = null;

    //Controlling
    public float mouseSensitivity = 2f;
    InputDevice cInput;

    [SerializeField]
    private float speed = 2.0f;
    
    //Camera
    [SerializeField]
    GameObject playerCamera=null;
    [SerializeField]
    GameObject crosshairCamera=null;
    private Vector3 desiredCrosshairPos;
    [SerializeField]
    GameObject crosshair=null;
    Vector3 crosshairOffset = Vector3.zero;
    RaycastHit rayHit;
    
    //Movement and Rotation
    Vector3 desiredMovement = Vector3.zero;
    Vector3 desiredRotation = Vector3.zero;


    void Start()
    {
        desiredRotation = transform.rotation.eulerAngles;
        desiredCrosshairPos = crosshair.transform.position;
        inventoryController = gameObject.GetComponent<InventoryController>();

        if (!GameController.GC.players[playerNumber].hasInputDevice)
        {
            switch (playerNumber)
            {
                case (sbyte)0:
                    GameController.IC.AssignPlayerToKeyboard(0);
                    crosshair.SetActive(true);
                    break;
                case (sbyte)1:
                    GameController.IC.AssignPlayerToController(1);
                    playerCamera.GetComponent<AudioListener>().enabled = false;
                    crosshair.SetActive(false);
                    crosshairCamera.SetActive(false);
                    break;
                default:
                    break;
            }
        }

        if(GameController.GC.playerAmount == 2)
        {
            switch (playerNumber)
            {
                case (sbyte)0:
                    playerCamera.GetComponent<Camera>().rect = new Rect(-0.5f, 0, 1, 1);
                    crosshairCamera.GetComponent<Camera>().rect = new Rect(-0.5f, 0, 1, 1);
                    playerCamera.GetComponent<Camera>().depth = 1;
                    crosshairCamera.GetComponent<Camera>().depth = 1.5f;
                    break;
                case (sbyte)1:
                    playerCamera.GetComponent<Camera>().rect = new Rect(0.5f, 0, 1, 1);
                    playerCamera.GetComponent<Camera>().depth = 2;
                    break;
                default:
                    break;
            }
        }

        if (!GameController.GC.players[playerNumber].inputDeviceIsKeyboard)
        {
            cInput = GameController.GC.players[playerNumber].currentInputDevice;
        }
    }

	void Update () 
    {
        if (dead)
            return;

        CheckInput();
        RayCast();
        GetComponent<AudioSource>().pitch = Time.timeScale;

        gameObject.GetComponent<CharacterController>().Move(desiredMovement * Time.deltaTime);
	}

    void LateUpdate()
    {
        if (rayHit.collider == null)
            return;
        var dirVector = rayHit.point - weaponHolder.transform.position;
        weaponHolder.transform.rotation = Quaternion.Slerp(weaponHolder.transform.rotation, Quaternion.LookRotation(dirVector), 12*Time.deltaTime);

    }

    public void RayCast()
    {
        Ray forwardRay = new Ray(transform.position, 
            transform.TransformDirection(Vector3.forward));
        Physics.Raycast(forwardRay, out rayHit, 1000.0f);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward));

        if (weaponHolder == null)
            return;

        Debug.DrawLine(transform.position, rayHit.point);
    }

    void CheckInput()
    {
        if (GameController.GC.players.Count == 0)
            return;

        if (GameController.GC.players[playerNumber].inputDeviceIsKeyboard)
            KeyboardInput();
        else if (GameController.GC.players[playerNumber].currentInputDevice != null)
            ControllerInput();
    }

    void KeyboardInput()
    {

        desiredMovement.x = Input.GetAxis("Horizontal") * speed;
        desiredMovement.z = Input.GetAxis("Vertical") * speed;

        if (Input.GetMouseButton(0))
            PrimaryAttack();

        if (Input.GetKeyDown(KeyCode.R))
            Reload();

        //Inventory
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetAxis("Mouse ScrollWheel") < 0)
            inventoryController.PreviousItem();
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetAxis("Mouse ScrollWheel") > 0)
            inventoryController.NextItem();

        CrosshairMovement();

        var lookPosition = new Vector3(crosshair.transform.position.x, transform.position.y, crosshair.transform.position.z);

        transform.LookAt(lookPosition);
    }

    void Reload()
    {
        var currentItem = gameObject.GetComponent<InventoryController>().currentItem;

        if(currentItem.GetComponent<IReloadable>() != null)
        {
            var itemToReload = currentItem.GetComponent<IReloadable>();
            itemToReload.Reload();
        }
    }

    void CrosshairMovement()
    {
        crosshairOffset.x += Input.GetAxis("Mouse X") * mouseSensitivity;
        crosshairOffset.z += Input.GetAxis("Mouse Y") * mouseSensitivity;

        desiredCrosshairPos =
            new Vector3(
                transform.position.x + crosshairOffset.x,
                1.0f,
                transform.position.z + crosshairOffset.z);


        crosshairOffset = Vector3.ClampMagnitude(crosshairOffset, 12.0f);

        crosshair.transform.position = desiredCrosshairPos;
    }

    void ControllerInput()
    {
        desiredMovement.x = cInput.LeftStickX * speed;
        desiredMovement.z = cInput.LeftStickY * speed;

        if (cInput.RightBumper || cInput.RightTrigger)
            PrimaryAttack();

        if (cInput.DPadRight)
            inventoryController.NextItem();

        else if (cInput.DPadLeft)
            inventoryController.PreviousItem();

        if(cInput.RightStickX != 0.0f || cInput.RightStickY != 0.0f)
        {
            float angle = Mathf.Atan2(cInput.RightStickX, cInput.RightStickY) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, angle, 0));
        }
        else if (desiredMovement != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, 
                Quaternion.LookRotation(desiredMovement), 
                speed * Time.deltaTime
                );
        }

        
    }

    void PrimaryAttack()
    {
        if(inventoryController.currentItem.GetComponent<IFireable>() != null)
        {
            inventoryController.currentItem.GetComponent<IFireable>().PrimaryFire(rayHit);
        }
    }

    public void Damage(float hitPoints)
    {
        if (dead)
            return;

        player_hp -= hitPoints;

        if (player_hp <= 0)
            Kill();
    }

    public void Kill()
    {
        if (dead)
            return;

        dead = !dead;
    }
}
