﻿using System.Collections;
using InControl;

public class Player
{

    //Controller
    public bool hasInputDevice = false;
    public InputDevice currentInputDevice = null;
    public bool inputDeviceIsKeyboard = false;

    public sbyte playerNumber = 0;

    public Player(sbyte playerN)
    {
        playerNumber = playerN;
    }

}
