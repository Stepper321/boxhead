﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class InventoryController : MonoBehaviour {

    public GameObject weaponHolder;
    public List<GameObject> inventory = new List<GameObject>();
    public GameObject currentItem;

    int currentWeaponIndex = 0;
	
	void Awake () 
    {
        GetFirstItem();
	}
	
    void GetFirstItem()
    {
        FindAndAddItem("Pistol", true);
        FindAndAddItem("Thompson", false);
    }

    void AddItemToInventory(GameObject item)
    {
        if(item.GetComponent<IPickup>() != null)
        {
            var instantiatedItem = Instantiate(item);
            instantiatedItem.GetComponent<IPickup>().pickupOwner = gameObject;
            inventory.Add(instantiatedItem);
            instantiatedItem.transform.parent = weaponHolder.transform;
            instantiatedItem.transform.localPosition = Vector3.zero;
            instantiatedItem.SetActive(false);
            inventory.Sort((s1, s2) => s1.GetComponent<IEquipable>().equipSlot.CompareTo(s2.GetComponent<IEquipable>().equipSlot));
        }
    }

    void FindAndAddItem(string item, bool equip)
    {
        var itemToEquip = GameController.GC.allPickupable.FirstOrDefault(x => x.GetComponent<IPickup>().PickUpName == item);
        
        if (itemToEquip != null)
        {
            Debug.Log("Adding item " + itemToEquip + " to inventory.");
            AddItemToInventory(itemToEquip);
            if(equip)
                Equip(item);
        }
        else
            Debug.Log("Couldn't find item " + itemToEquip);
    }

    public void NextItem()
    {
        if (currentWeaponIndex < inventory.Count -1)
            currentWeaponIndex++;
        else
            currentWeaponIndex = 0;
        Debug.Log(currentWeaponIndex);
        Equip(currentWeaponIndex);
    }

    public void PreviousItem()
    {
        if (currentWeaponIndex == 0)
            currentWeaponIndex = inventory.Count - 1;
        else
            currentWeaponIndex--;
        Debug.Log(currentWeaponIndex);
        Equip(currentWeaponIndex);
    }

    void Equip(int itemIndex)
    {
        var itemToEquip = inventory[itemIndex];

        if (itemToEquip == null)
            return;

        foreach (var equipable in inventory)
        {
            if (equipable == itemToEquip)
            {
                Debug.Log("Equpping item " + itemToEquip.name);
                equipable.SetActive(true);
                currentItem = equipable;
            }
            else
            {
                equipable.SetActive(false);
            }
        }
    }

    void Equip(string pickupName)
    {
        var itemToEquip = inventory.FirstOrDefault(x => x.GetComponent<IPickup>().PickUpName == pickupName);
        if (itemToEquip == null)
            return;

            foreach(var equipable in inventory)
            {
                if (pickupName == equipable.GetComponent<IPickup>().PickUpName)
                {
                    Debug.Log("Equpping item " + itemToEquip.name);
                    equipable.SetActive(true);
                    currentItem = equipable;
                }
                else
                {
                    equipable.SetActive(false);
                }
            }
    }
	
}
