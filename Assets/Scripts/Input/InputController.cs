﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using InControl;

public class InputController : MonoBehaviour {

    public InputDevice[] inputDevices = new InputDevice[1];
    public GameObject[] playerSelector;
    public bool[] inputDeviceAssigned = new bool[1];
    public bool playerHasKeyboard = false;
    public bool assigningControllersOnMainMenu = false;
    bool playerAssigning = false;
    bool player1Assigned = false;
    bool player2Assigned = false;
    

    void Start()
    {
        MainMenuInit();
    }

    void Update()
    {
        var inputDevice = InputManager.ActiveDevice;

        if (assigningControllersOnMainMenu)
        {
            AssignPlayers();
        }
    }

    //For assigning players. If the game is started without assigning controllers, the first Player will have the input with the Keyboard, and the second player with the controller.
    void AssignPlayers()
    {
        if(player1Assigned && player2Assigned)
        {

        }

        //If Player1 is not assigned, and neither is player2, and the player is assigning on the Main Menu.
        if (!player1Assigned && !player2Assigned)
            playerSelector[2].GetComponent<Text>().text = "First player, please press start or enter to initialize your preferred input device.";
        else if (player1Assigned && !player2Assigned)
            playerSelector[2].GetComponent<Text>().text = "Second player, please press start or enter to initialize your preferred input device.\nFirst player, if you're lonely just press start or enter to continue.";
        else if (player1Assigned && player2Assigned)
            playerSelector[2].GetComponent<Text>().text = "All players assigned, press start to continue!.";

        if (playerAssigning)
            return;

        //Check for the Enter button, and if player 1 is NOT assigned. See the exclamation mark.
        if (Input.GetKeyDown(KeyCode.Return) && !player1Assigned)
            AssignPlayer1(false);
        //Same as the above, but then getting if MenuWasPressed. (i.e. the start button on Controllers.)
        else if (InputManager.ActiveDevice.MenuWasPressed && !player1Assigned)
            AssignPlayer1(true);

        //Return if player 1 is not assigned, so the code won't continue.
        if (!player1Assigned)
            return;

        //If Enter is down, player 2 is not assigned and a player does NOT have a keyboard already.
        if (Input.GetKeyDown(KeyCode.Return) && !player2Assigned && !playerHasKeyboard)
            AssignPlayer2(false);
        else if (InputManager.ActiveDevice.MenuWasPressed && !player2Assigned)
            AssignPlayer2(true);
    }

    //This is for getting everything back to the default value. This gets called everytime a GUI button gets pressed.
    public void MainMenuInit()
    {
        
        player1Assigned = false;
        player2Assigned = false;
        playerHasKeyboard = false;
        //GameController.GC.playerAmount = 1;
        if (!assigningControllersOnMainMenu)
            return;
        playerSelector[0].GetComponent<Text>().text = "press Start on the controller or Enter on the keyboard to continue.";
        playerSelector[1].GetComponent<Text>().text = "press Start on the controller or Enter on the keyboard to continue.";
        playerSelector[2].GetComponent<Text>().text = "";
    }


    void AssignPlayer1(bool inputDeviceIsController)
    {
        //Return if already assigning a player. (So this doesn't get called twice, or more..)
        if (playerAssigning)
            return;

        playerAssigning = true;

        //SWITCH STATE HAHAUEHAUEHAUEAUHEU
        switch(inputDeviceIsController)
        {
            case true:
                GameController.IC.AssignPlayerToController(0);
                playerSelector[0].GetComponent<Text>().text = "assigned to Controller.";
                break;
            case false:
                GameController.IC.AssignPlayerToKeyboard(0);
                playerSelector[0].GetComponent<Text>().text = "assigned to Keyboard.";
                break;
        }

        //Invoke method, because I was too lazy to get a timer.
        Invoke("Player1Assigned", .01f);
        
    }

    void Player1Assigned()
    {
        playerAssigning = false;
        player1Assigned = true;
        GameController.GC.playerAmount = 1;
    }

    void AssignPlayer2(bool inputDeviceIsController)
    {
        if (playerAssigning)
            return;

        playerAssigning = true;

        switch (inputDeviceIsController)
        {
            case true:
                GameController.IC.AssignPlayerToController(1);
                playerSelector[1].GetComponent<Text>().text = "assigned to Controller.";
                break;
            case false:
                GameController.IC.AssignPlayerToKeyboard(1);
                playerSelector[1].GetComponent<Text>().text = "assigned to Keyboard.";
                break;
        }

        Invoke("Player2Assigned", .01f);
        
    }

    void Player2Assigned()
    {
        playerAssigning = false;
        player2Assigned = true;
        GameController.GC.playerAmount = 2;
    }


    public void AssignPlayerToKeyboard(sbyte playern)
    {
        if (playerHasKeyboard)
            return;

        //HEEEY HAHAHAHAHAHAHAHAHAH FUCKER.
        GameController.GC.players[playern].hasInputDevice = true;
        GameController.GC.players[playern].inputDeviceIsKeyboard = true;
        GameController.GC.players[playern].currentInputDevice = null;
        
        playerHasKeyboard = true;
    }

    public void AssignPlayerToController(sbyte playern)
    {
        GameController.GC.players[playern].hasInputDevice = true;
        GameController.GC.players[playern].inputDeviceIsKeyboard = false;
        GameController.GC.players[playern].currentInputDevice = InputManager.ActiveDevice;
    }

    //Don't destroy on load, so the.. yeah. You understand right? Haha. Fuck off dickwad.
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
