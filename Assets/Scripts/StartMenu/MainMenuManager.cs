﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class MainMenuManager : MonoBehaviour {

    public GameObject[] pages;
    private int currentPage = 0;
    public static GameObject mainMenuManager;
    private List<int> pagesOpened = new List<int>();
    bool addToList = true;

    //SerializedFields
    [SerializeField]
    private float slowMotionSpeed = 0.1f;
    [SerializeField]
    private GameObject MainMenu=null;

    private bool mainMenuActive = true;

    private void Start()
    {
        Time.timeScale = 0.1f;
        Time.fixedDeltaTime = slowMotionSpeed * 0.02f;
        mainMenuManager = gameObject;
        pagesOpened.Add(0);
        GameController.IC.assigningControllersOnMainMenu = true;
    }

	public void Open_Page(int pageNumber)
    {
        ClosePages();
        TurnOffMainMenu();
        foreach (var page in pages)
        {
            if (page.GetComponent<Page>().pageID == pageNumber)
            {
                page.SetActive(true);

                if (addToList)
                    pagesOpened.Add(page.GetComponent<Page>().pageID);

                currentPage = page.GetComponent<Page>().pageID;
                addToList = true;
            }   
        }

        Debug.Log(currentPage);
    }

    void ClosePages()
    {
        foreach (var page in pages)
        {
            page.SetActive(false);
        }
    }

    public void Update()
    {
        switch(mainMenuActive)
        {
            case true:
                if (MainMenu)
                    MainMenu.SetActive(true);
                break;
            case false:
                if (MainMenu)
                    MainMenu.SetActive(false);
                break;
        }

        if (!mainMenuActive)
        {
            if (InputManager.ActiveDevice.Action2 || Input.GetKeyDown(KeyCode.Escape))
            {
                ClosePages();
                TurnOnMainMenu();
            }
        }
        else
            ClosePages();

    }

    public void TurnOffMainMenu()
    {
        mainMenuActive = false;
    }

    public void TurnOnMainMenu()
    {
        mainMenuActive = true;
    }

    public void Return_Page()
    {

        pagesOpened.Remove(pagesOpened[pagesOpened.Count - 1]);
        addToList = false;
        Open_Page(pagesOpened[pagesOpened.Count - 1]);
    }


    public void Quit_Game()
    {
        Application.Quit();
    }

    public void Credits()
    {

    }
}
