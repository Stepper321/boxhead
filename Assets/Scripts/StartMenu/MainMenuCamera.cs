﻿using UnityEngine;
using System.Collections;

public class MainMenuCamera : MonoBehaviour {

    [SerializeField]
    private Transform follow=null;

    [SerializeField]
    private float smoothSpeed = 2.0f;

    [SerializeField]
    private Vector3 offset = Vector3.zero;

	void Start () 
    {
        transform.position = follow.position + offset;
	}
	
	
	void Update () 
    {
        transform.position = Vector3.Lerp(
            transform.position, 
            follow.position + offset, smoothSpeed * Time.deltaTime
            );
	}
}
