﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    [SerializeField]Vector3 offset = Vector3.zero;
    GameObject playerToFollow;

    [SerializeField]
    private float smoothTime = 3.0f;
	
	void Start () 
    {
        //Find all players.
        GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("Player");
        //Foreach is quite simple, right? For every player in the array allPlayers that's created up here.
        foreach(var player in allPlayers)
        {
            //If that player does NOT have a camera.
            //This also Gets the component of the Player, a script called PlayerController
            //Steps:
            //Follow that player.
            //Set the player it's following's hasCamera bool to true.
            //Break out of the loop.
            if (!player.GetComponent<PlayerController>().hasCamera)
            {
                playerToFollow = player;
                player.GetComponent<PlayerController>().hasCamera = true;
                break;
            }
            
        }
	}
	
	
	void Update () 
    {
        if (!playerToFollow)
            return;

        transform.position = Vector3.Slerp(transform.position, 
            new Vector3(playerToFollow.transform.position.x, 
                playerToFollow.transform.position.y, 
                playerToFollow.transform.position.z) + offset, 
            smoothTime * Time.deltaTime);
	
	}

    //public void FindPlayers()
    //{
        
    //}
}
