﻿using UnityEngine;
using System.Collections;

public enum FireType
{
    SemiAutomatic,
    Automatic,
    Manual
}

public enum BulletType
{
    Normal,
    Shotgun,
    Laser
}


public class Weapon : MonoBehaviour, IFireable, IEquipable, IReloadable, IPickup
{
    public string weaponName = string.Empty;
    public float fireRate = 0.3f;
    public float reloadSpeed = 1.5f;
    public int baseDamage = 12;
    public int bulletPerShot = 1;
    public int maxClip = 12;
    public int currentClip = 12;
    public sbyte weaponSlot = 1;
    public bool canReload = true;
    public bool charge = false;
    public FireType fireType = FireType.SemiAutomatic;
    public BulletType bulletType = BulletType.Normal;
    public Transform bulletExit = null;
    public AudioClip[] gunSound = null;
    public AudioClip dryFire = null;

    bool clicked = false;

    //private variables
    bool canShoot = true;
    LineRenderer lc;
    bool reloading = false;
    AudioSource audioSource;

    void LateUpdate()
    {
        clicked = Input.GetMouseButton(0);
        lc.SetPosition(0, bulletExit.position);
    }

    void Start()
    {
        Debug.Log(gunSound.Length);
        lc = gameObject.AddComponent<LineRenderer>();
        lc.material = new Material(Shader.Find("Particles/Additive"));
        lc.SetColors(Color.yellow, Color.yellow);
        lc.SetVertexCount(2);
        lc.SetPosition(1, Vector3.forward);
        lc.SetWidth(0.2f, 0.1f);
        lc.enabled = false;
        audioSource = pickupOwner.GetComponent<AudioSource>();
    }

    public void PrimaryFire(RaycastHit hit)
    {
        if (reloading)
            return;

        if (currentClip == 0 && canShoot)
        {
            DryFire();
            canShoot = false;
            StartCoroutine(FireDelayWait());
            return;
        }

        if (canShoot)
        {
            if (fireType == FireType.SemiAutomatic)
            {
                if(clicked != Input.GetMouseButton(0))
                    PrimaryShoot(hit);
            }

            if(fireType == FireType.Automatic)
            {
                PrimaryShoot(hit);
            }
            
        }
    }

    void DamageTest(RaycastHit hit)
    {
        if(hit.collider != null)
        {
            var damageApplier = hit.collider.gameObject.GetComponent<IDamageAble>();
            if (damageApplier != null)
                damageApplier.Damage(baseDamage);
        }
    }

    public void PrimaryShoot(RaycastHit hit)
    {
        audioSource.PlayOneShot(gunSound[Random.Range(0, gunSound.Length - 1)], 0.5f);
        currentClip -= bulletPerShot;
        canShoot = false;
        lc.enabled = true;
        lc.SetPosition(0, bulletExit.position);

        if (hit.collider != null)
        {
            lc.SetPosition(1, hit.point);
        }
        else
        {
            lc.SetPosition(1, bulletExit.position + transform.forward * 100);
        }
        DamageTest(hit);
        StartCoroutine(gunEffects());
        StartCoroutine(FireDelayWait());
    }

    public void DryFire()
    {
        audioSource.PlayOneShot(dryFire);
    }

    public void Reload()
    {
        if (currentClip >= maxClip)
            return;

        StartCoroutine(Reloading());
    }

    public void SecondaryFire()
    {
        
    }

    #region IEnumerators
    IEnumerator Reloading()
    {
        reloading = true;
        yield return new WaitForSeconds(reloadSpeed);
        reloading = false;
        currentClip = maxClip;
    }
    IEnumerator gunEffects()
    {
        yield return new WaitForSeconds(0.05f);
        lc.enabled = false;
    }
    IEnumerator FireDelayWait()
    {
        yield return new WaitForSeconds(fireRate);
        canShoot = true;
    }
    #endregion

    public string PickUpName
    {
        get { return weaponName; }
    }

    #region useless for now
    public void Equip()
    {
        
    }

    public void DeEquip()
    {
        
    }

    public void Pickup()
    {
        
    }

    public void Drop()
    {
        
    }
    #endregion

    public GameObject pickupOwner { get; set; }
    public sbyte equipSlot { get { return weaponSlot; } set { weaponSlot = value; } }
}
