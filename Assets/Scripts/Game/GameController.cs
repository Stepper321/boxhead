﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    //Singleton?
    public static GameObject gameController;
    public static GameController GC;
    public static InputController IC;

    //Players
    public sbyte playerAmount = 1;
    bool choosingPlayer = false;

    //Lists...
    public List<Player> players = new List<Player>();

    public List<GameObject> allPickupable = new List<GameObject>();

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        gameController = gameObject;
        GC = gameController.GetComponent<GameController>();
        IC = gameController.GetComponent<InputController>();

        players.Add(new Player(0));
        players.Add(new Player(1));

        foreach (var seperate in players)
        {
            seperate.hasInputDevice = false;
            seperate.currentInputDevice = null;
            seperate.inputDeviceIsKeyboard = false;
        }

        StartGame();
    }

    void InitializeAllPickups()
    {
        foreach (GameObject pickup in Resources.LoadAll("Pickupables"))
        {
            IPickup pickupToAdd = pickup.GetComponent(typeof(IPickup)) as IPickup;
            Debug.Log("Found " + pickupToAdd.PickUpName);
            if(pickupToAdd != null)
                allPickupable.Add(pickup);
        }
    }

    void StartGame()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
        InitializeAllPickups();
    }

}
