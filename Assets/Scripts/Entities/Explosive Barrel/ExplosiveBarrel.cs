﻿using UnityEngine;
using System.Collections;

public class ExplosiveBarrel : MonoBehaviour, IDamageAble {

    [SerializeField]
    private GameObject healthyBarrel=null;
    [SerializeField]
    private GameObject damagedBarrel=null;
    [SerializeField]
    private GameObject[] explosionParticle=null;
    [SerializeField]
    private GameObject collisionObject=null;
    [SerializeField]
    private AudioClip[] explosionSFX = null;

    //remove this later
    public bool damage = false;
    float health = 40;
    private bool destroyed = false;

    enum DamageTypes
    {
        Unknown,
        Healthy,
        Damaged,
        Destroyed
    }

    DamageTypes damageTypes = new DamageTypes();

	
	void Start () 
    {
        damageTypes = DamageTypes.Healthy;
	}
	
	
	void Update () 
    {
	    if(damage)
        {
            Damage(0);
            damage = !damage;
        }

        if (destroyed)
            transform.position = new Vector3(transform.position.x, transform.position.y - 2.5f * Time.deltaTime, transform.position.z);
	}

    public void Damage(float hitPoints)
    {
        if (!healthyBarrel || !damagedBarrel || !explosionParticle[0])
            gameObject.SetActive(false);

        switch(damageTypes)
        {
            case DamageTypes.Healthy:
                healthyBarrel.SetActive(false);
                damagedBarrel.SetActive(true);
                damageTypes = DamageTypes.Damaged;
                break;
            case DamageTypes.Damaged:
                Kill();
                damageTypes = DamageTypes.Destroyed;
                break;
            default:
                //Do absolutely FUCK ALL.   
                break;
        }

        return;
        
    }

    public void Kill()
    {
        if (!healthyBarrel || !damagedBarrel || !explosionParticle[0])
            gameObject.SetActive(false);

        destroyed = true;
        //damagedBarrel.SetActive(false);
        healthyBarrel.SetActive(false);
        int explosion = Random.Range(0, explosionParticle.Length);
        Debug.Log(explosionParticle.Length);
        explosionParticle[explosion].SetActive(true);
        explosionParticle[explosion].GetComponent<ParticleSystem>().Play(true);
        GetComponent<AudioSource>().PlayOneShot(explosionSFX[Random.Range(0, explosionSFX.Length)]);
        gameObject.GetComponent<MeshCollider>().enabled = false;
        Destroy(gameObject, 5.0f);
        return;
    }

    
}
