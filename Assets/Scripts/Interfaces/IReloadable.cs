﻿using System;

public interface IReloadable  
{
    void Reload();
}
