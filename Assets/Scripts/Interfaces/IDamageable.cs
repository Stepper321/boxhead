﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


interface IDamageAble
{
    void Damage(float hitPoints);
    void Kill();
}
