﻿using System;

public interface IThrowable 
{
    void Throw();
}
