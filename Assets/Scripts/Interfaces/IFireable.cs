﻿using System;
using UnityEngine;

public interface IFireable 
{
    void PrimaryFire(RaycastHit hit);
    void DryFire();
    void SecondaryFire();
}
