﻿using System;

public interface IEquipable 
{
    void Equip();
    void DeEquip();
    sbyte equipSlot
    {
        get;
        set;
    }
}
