﻿using System;
using UnityEngine;

public interface IPickup 
{
    void Pickup();
    void Drop();
    string PickUpName
    {
        get;
    }
    GameObject pickupOwner
    {
        get;
        set;
    }
}
